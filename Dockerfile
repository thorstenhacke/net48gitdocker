#BaseImage with .Net 4.8 SDK MSBuild, Nuget etc
FROM mcr.microsoft.com/dotnet/framework/sdk:4.8
#Download install script from Chocolatey
RUN powershell "(new-object System.Net.WebClient).DownloadFile('https://chocolatey.org/install.ps1', 'installChoco.ps1')"
#Excecute Install-Script to get Choco
RUN powershell -File installChoco.ps1
#Use Choco to Install GIT
RUN choco install git.install -y